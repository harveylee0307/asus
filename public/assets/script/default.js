// Minified version of isMobile included in the HTML since it's small
!function (a) { var b = /iPhone/i, c = /iPod/i, d = /iPad/i, e = /(?=.*\bAndroid\b)(?=.*\bMobile\b)/i, f = /Android/i, g = /(?=.*\bAndroid\b)(?=.*\bSD4930UR\b)/i, h = /(?=.*\bAndroid\b)(?=.*\b(?:KFOT|KFTT|KFJWI|KFJWA|KFSOWI|KFTHWI|KFTHWA|KFAPWI|KFAPWA|KFARWI|KFASWI|KFSAWI|KFSAWA)\b)/i, i = /IEMobile/i, j = /(?=.*\bWindows\b)(?=.*\bARM\b)/i, k = /BlackBerry/i, l = /BB10/i, m = /Opera Mini/i, n = /(CriOS|Chrome)(?=.*\bMobile\b)/i, o = /(?=.*\bFirefox\b)(?=.*\bMobile\b)/i, p = new RegExp("(?:Nexus 7|BNTV250|Kindle Fire|Silk|GT-P1000)", "i"), q = function (a, b) { return a.test(b) }, r = function (a) { var r = a || navigator.userAgent, s = r.split("[FBAN"); return "undefined" != typeof s[1] && (r = s[0]), s = r.split("Twitter"), "undefined" != typeof s[1] && (r = s[0]), this.apple = { phone: q(b, r), ipod: q(c, r), tablet: !q(b, r) && q(d, r), device: q(b, r) || q(c, r) || q(d, r) }, this.amazon = { phone: q(g, r), tablet: !q(g, r) && q(h, r), device: q(g, r) || q(h, r) }, this.android = { phone: q(g, r) || q(e, r), tablet: !q(g, r) && !q(e, r) && (q(h, r) || q(f, r)), device: q(g, r) || q(h, r) || q(e, r) || q(f, r) }, this.windows = { phone: q(i, r), tablet: q(j, r), device: q(i, r) || q(j, r) }, this.other = { blackberry: q(k, r), blackberry10: q(l, r), opera: q(m, r), firefox: q(o, r), chrome: q(n, r), device: q(k, r) || q(l, r) || q(m, r) || q(o, r) || q(n, r) }, this.seven_inch = q(p, r), this.any = this.apple.device || this.android.device || this.windows.device || this.other.device || this.seven_inch, this.phone = this.apple.phone || this.android.phone || this.windows.phone, this.tablet = this.apple.tablet || this.android.tablet || this.windows.tablet, "undefined" == typeof window ? this : void 0 }, s = function () { var a = new r; return a.Class = r, a }; "undefined" != typeof module && module.exports && "undefined" == typeof window ? module.exports = r : "undefined" != typeof module && module.exports && "undefined" != typeof window ? module.exports = s() : "function" == typeof define && define.amd ? define("isMobile", [], a.isMobile = s()) : a.isMobile = s() }(this);


$(function () {
  if (isMobile.any) {
    $('body').addClass('isMobile');
  }

  if (isMobile.apple.device) {
    $('body').addClass('isAppleDevice');
  }
  initNav();
  initSlider();
  initPopup();
  initBlogItem();
  initVideoPopup();
  initShareUrl();
  $(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
      $('#btn-back-top').fadeIn();
    } else {
      $('#btn-back-top').fadeOut();
    }
  });
  $('#btn-back-top').click(function () {
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
  });
});

function initShareUrl () {
  $('body').on('click', '.social-share__link--link', function () {
    $('.social-share__copy').css('display', 'flex');
  });
  $('body').on('click', '.social-share__copy .btn-close', function () {
    $('.social-share__copy').hide();
  });
  $('body').on('click', '.social-share__copy .btn-copy', function () {
    $('.copy-area').select();
    document.execCommand('copy');
  });
}

function openVideoPopup (id) {
  var target = $('.popup-video[href="' + id + '"]')

  var video_url = target.data('video-url');
  var share_url = target.data('share-url');
  var isVertical = target.data('vertical');
  isVertical ? isVertical = 'mfp-vertical' : isVertical = '';

  $.magnificPopup.open({
    fixedContentPos: true,
    mainClass: isVertical,
    items: {
      type: 'inline',
      src: `<div class="mfp-video-block">
      <div class="social-share">
      <div class="social-share__list">
        <div class="social-share__item ">
          <a href="javascript:;" data-share-type="fb" class="social-share__link social-share__link--fb" target="_blank"></a>
        </div>
        <div class="social-share__item ">
          <a href="javascript:;" data-share-type="line" class="social-share__link social-share__link--line" target="_blank"></a>
        </div>
        <div class="social-share__item ">
          <a href="javascript:;" data-share-type="link" class="social-share__link social-share__link--link" ></a>
        </div>
      </div>
      
      <div class="social-share__copy">
      <button class="btn-copy"></button>
      <input class="copy-area" type="text" readonly>
      <button class="btn-close"></button>
      </div>
      </div>
      <video controls preload="auto">
      <source src="${video_url}" type="video/mp4">
      </video>
      </div>`,

    },
    callbacks: {

      open: function () {
        // var page_Url = window.location.href;
        var $this = $(this.content);
        $this.find('video')[0].play();
        $this.find('.social-share__link--fb').attr('href', 'https://www.facebook.com/sharer.php?u=' + share_url);
        $this.find('.social-share__link--line').attr('href', 'https://lineit.line.me/share/ui?url=' + share_url);
        $this.find('.social-share__copy .copy-area').val(share_url);

      },
      close: function () {
        $(this.content).find('video')[0].load();
      }
    }
  });
}
function initVideoPopup () {
  var hash = window.location.hash;
  if (window.location.search.includes('%23')) {
    hash = '#' + window.location.search.split('%23')[1];
  }
  if (hash != '') {
    openVideoPopup(hash);
  }

  //var hash = window.location.hash || '#' + window.location.search.split('%23')[1];
  //if (hash) {
  //    openVideoPopup(hash);
  //}
  $('.popup-video').on('click', function () {
    var id = $(this).attr('href');
    openVideoPopup(id);
  });
}
function initPopup () {
  $('.shots__item').on('click', function () {
    $(this).children('.popup-player').trigger('click');
  });

  $('.popup-player').magnificPopup({
    type: 'iframe',
    mainClass: 'mfp-fade',
    fixedContentPos: true,
    iframe: {
      srcAction: 'iframe_src',
      markup: `
      <div class="mfp-video-block">
      <div class="mfp-iframe-scaler">
        <div class="mfp-close"></div>
        <iframe class="mfp-iframe" src="//about:blank" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        </div>`,
      patterns: {
        youtube: {
          index: 'youtube.com/',
          id: 'v=',
          src: 'https://www.youtube.com/embed/%id%?autoplay=1'
        },
      }
    },
    callbacks: {
      beforeOpen: function () {

        var $triggerEl = $(this.st.el);
        console.log();

        if ($triggerEl.data('vertical')) {
          this.st.mainClass = this.st.mainClass + ' ' + 'mfp-vertical';
        }

      }
    }
  }
  );
  $('.master-gallery').each(function () {
    $(this).magnificPopup({
      delegate: 'a',
      type: 'image',
      gallery: {
        enabled: true
      },
      zoom: {
        enabled: true,
        duration: 300,
        easing: 'ease-in-out',
        opener: function (openerElement) {
          return openerElement.find('img');
        }
      }
    });
  });


  $('.users-slider__item').on('click', function () {
    $(this).children('.userGallery').magnificPopup('open');
  });

  $('.userGallery').each(function () {
    $(this).magnificPopup({
      delegate: 'a',
      type: 'image',
      gallery: {
        enabled: true,
        navigateByImgClick: true
      },
      fixedContentPos: false
    });
  });


  $('.popup-modal').on('click', function (e) {
    e.preventDefault();
    var open_tg = $(this).attr('href');
    $(open_tg).css('display', 'flex');
    $('.modal-list').slick('setPosition');
  });

  $('.modal-instruction .mfp-close').on('click', function (e) {
    $(this).parent().css('display', 'none');
  });

  $('.modal-instruction').on('click', function (e) {
    var _con = $('.modal-instruction .popup-player');
    var _con2 = $('.slick-arrow');

    if (!_con.is(e.target) && !_con2.is(e.target)) {
      $(this).css('display', 'none');
    }

  });

}

function initSlider () {
  $('.main-slider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    adaptiveHeight: true,
    lazyLoad: "progressive",
    speed: 600,
    arrows: true,
    dots: true,
  });

  $('.master-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    infinite: false,
    lazyLoad: "progressive",
    speed: 600,
    arrows: true,
    dots: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  });


  $('.modal-list').slick({
    slidesPerRow: 2,
    rows: 3,
    infinite: false,
    lazyLoad: "progressive",
    speed: 600,
    arrows: true,
    dots: false,
  });


  $('.users-slider').slick({
    slidesPerRow: 3,
    rows: 2,
    infinite: false,
    lazyLoad: "progressive",
    speed: 600,
    arrows: true,
    adaptiveHeight: true,
    dots: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesPerRow: 2,
          rows: 2,
        }
      }
    ]
  });

  $('.blog-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    infinite: false,
    lazyLoad: "progressive",
    speed: 600,
    arrows: true,
    dots: true,
    responsive: [
      {
        breakpoint: 768,
        settings: "unslick"
      }
    ]
  });


  $(window).on('load resize', function () {
    if ($(window).width() > 768) {
      if ($('.shots').hasClass('slick-initialized')) {
        $('.shots').slick('unslick');
      }
      if (!$('.blog-slider').hasClass('slick-initialized')) {
        $('.blog-slider').slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: false,
          lazyLoad: "progressive",
          speed: 600,
          arrows: true,
          dots: true,
          responsive: [
            {
              breakpoint: 768,
              settings: "unslick"
            }
          ]
        });
      }
      return
    }
    if (!$('.shots').hasClass('slick-initialized')) {
      return $('.shots').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: '50px',
        arrows: true,
        dots: false,
        mobileFirst: true
      });
    }
  });


}
function initBlogItem () {
  var $item = $('.blog-slider__item');

  if ($(window).width() < 768) {
    $item.slice(3).hide();
  }

  $('.more-blog').on('click', function () {
    $item.show();
    $(this).hide();
  });

  $(window).on('load', function () {
    if ($(window).width() < 768) {
      $item.slice(3).hide();
      $('.more-blog').show();
    } else {
      $item.show();
    }
  });

}
function initNav () {

  var sections = $('section');
  var nav = $('nav');
  var nav_height = nav.outerHeight();

  $(window).on('scroll', function () {
    var cur_pos = $(this).scrollTop();

    if ($(window).scrollTop() >= nav_height) {
      $('.menu').addClass('fixed');

    } else {
      $('.menu').removeClass('fixed');

    }

    sections.each(function () {
      var top = $(this).offset().top - (nav_height * 1.5);
      var bottom = top + $(this).outerHeight();

      if (cur_pos >= top && cur_pos <= bottom) {
        $('.menu__list .item').find('a').removeClass('active');
        sections.removeClass('active');
        $(this).addClass('active');
        $('.menu__list .item').find('a[href="#' + $(this).attr('id') + '"]').addClass('active');
      } else if (cur_pos < top) {
        $('.menu__list .item').find('a[href="#' + $(this).attr('id') + '"]').removeClass('active');
      }
    });
  });

  $('.menu__list .item').find('a').on('click', function (e) {
    e.preventDefault();
    var $el = $(this);
    var id = $el.attr('href');
    $('html, body').animate({
      scrollTop: $(id).offset().top - nav_height
    }, 500);

    return false;
  });

  $('.burger-menu').click(function () {
    $(this).toggleClass('menu-on');
    $('.menu__list').toggleClass('menu-on');
  });
  $(window).on('load resize scroll', function () {
    if ($(window).width() > 1024) {
      $('.menu__list').addClass('menu-on');
    } else {
      $('.burger-menu , .menu__list').removeClass('menu-on');
    }

  });

}
